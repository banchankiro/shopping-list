import React from "react";
import logo from "../../logo.svg";
import "../../App.css";

//Stateless Functional Component

const Navbar = ({ totalItems }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-dark">
      <img src={logo} className="App-logo" alt="logo" />
      <span className="text-light">Shopping List</span>
      <span className="badge badge-pill badge-secondary">{totalItems}</span>
    </nav>
  );
};

export default Navbar;
