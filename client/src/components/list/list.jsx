import React, { Component } from "react";
import ListItem from "./listItem/listItem";
import "./list.css";

class List extends Component {
  render() {
    const {
      onDelete,
      onNameChange,
      onQtyChange,
      onChecked,
      onAdd,
      onClearChecked,
      listItems
    } = this.props;
    return (
      <div className="list">
        {listItems.map(listItem => (
          <ListItem
            key={listItem.id}
            onDelete={onDelete}
            onNameChange={onNameChange}
            onQtyChange={onQtyChange}
            onChecked={onChecked}
            listItem={listItem}
          />
        ))}
        <button onClick={onAdd} className="btn btn-primary btn-sm m-2">
          Add
        </button>
        <button onClick={onClearChecked} className="btn btn-warning btn-sm m-2">
          Clear Checked
        </button>
      </div>
    );
  }
}

export default List;
