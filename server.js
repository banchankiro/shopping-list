const express = require("express");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json({ extended: false });

const app = express();

var customers = [];

app.get("/api/customers", (req, res) => {
  res.json(customers);
});

app.post("/api/customers/change", jsonParser, function(req, res) {
  customers.forEach(function(customer) {
    if (customer.id === req.body.id) {
      customer.itemName = req.body.itemName;
      customer.itemQty = req.body.itemQty;
      customer.checked = req.body.checked;
    }
  });
});

app.post("/api/customers", jsonParser, function(req, res) {
  customers.push(req.body);
  console.log(req.body);
});

app.delete("/api/customers/:itemId", function(req, res) {
  for (var i = 0; i < customers.length; i++) {
    if (customers[i].id === req.params.itemId) customers.splice(i, 1);
  }
});

const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
