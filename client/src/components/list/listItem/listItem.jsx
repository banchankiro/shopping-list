import React, { Component } from "react";
import "./listItem.css";

class ListItem extends Component {
  constructor(props, currentRef) {
    super(props, currentRef);
    this.currentRef = React.createRef();
  }

  componentDidMount() {
    const currentItem = this.currentRef.current;
    const children = currentItem.children;
    if (this.props.listItem.checked) {
      children[1].style.backgroundColor = "#e9ecef";
      children[1].style.color = "rgba(0, 0, 0, 0.4)";
      children[2].style.backgroundColor = "#e9ecef";
      children[2].style.color = "rgba(0, 0, 0, 0.4)";
    } else {
      children[1].style.backgroundColor = "#fff";
      children[1].style.color = "#495057";
      children[2].style.backgroundColor = "#fff";
      children[2].style.color = "#495057";
    }
  }

  componentWillUnmount() {
    const deleteData = this.props.listItem.id;
    fetch("/api/customers/" + deleteData, {
      method: "DELETE",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(res => res.json())
      .then(data => console.log(data))
      .catch(err => console.log(err));
  }

  render() {
    const {
      onDelete,
      onNameChange,
      onQtyChange,
      onChecked,
      listItem
    } = this.props;
    return (
      <div
        className="listItem text-center input-group input-group-sm m-1 border-bottom"
        ref={this.currentRef}
      >
        <div className="input-group-prepend">
          <span className="input-group-text border-0">
            <input
              onChange={e => onChecked(listItem, e)}
              type="checkbox"
              checked={listItem.checked}
            />
          </span>
        </div>
        <input
          className="form-control border-0"
          type="text"
          defaultValue={listItem.itemName}
          placeholder="Name of the Item"
          onBlur={e => onNameChange(listItem, e)}
        />
        <input
          className="form-control border-0"
          type="text"
          defaultValue={listItem.itemQty}
          placeholder="Quantity"
          onBlur={e => onQtyChange(listItem, e)}
        />
        <div
          onClick={() => onDelete(listItem)}
          style={{ cursor: "default" }}
          className="input-group-append"
        >
          <span className="input-group-text border-0 delete">&times;</span>
        </div>
      </div>
    );
  }
}

export default ListItem;
