import React, { Component } from "react";
import "./App.css";
import List from "./components/list/list";
import Navbar from "./components/navBar/navBar";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listItems: []
    };
  }
  componentDidMount = () => {
    fetch("api/customers")
      .then(response => response.json())
      .then(listItems => this.setState({ listItems }))
      .catch(err => console.log(err));
  };

  handleDelete = listItem => {
    const listItems = this.state.listItems.filter(c => c.id !== listItem.id);
    this.setState({ listItems });
  };

  handleNameChange = (listItem, e) => {
    const listItems = [...this.state.listItems];
    const index = listItems.indexOf(listItem);
    if (!(e.target.value === listItems[index].itemName)) {
      listItems[index].itemName = e.target.value;
      this.setState({ listItems });
      fetch("api/customers/change", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-type": "application/json"
        },
        body: JSON.stringify(listItems[index])
      })
        .then(res => res.json())
        .then(data => console.log(data))
        .catch(err => console.log(err));
    }
  };

  handleQtyChange = (listItem, e) => {
    const listItems = [...this.state.listItems];
    const index = listItems.indexOf(listItem);
    if (!(e.target.value === listItems[index].itemName)) {
      listItems[index].itemQty = e.target.value;
      this.setState({ listItems });
      fetch("api/customers/change", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-type": "application/json"
        },
        body: JSON.stringify(listItems[index])
      })
        .then(res => res.json())
        .then(data => console.log(data))
        .catch(err => console.log(err));
    }
  };

  handleChecked = (listItem, e) => {
    const listItems = [...this.state.listItems];
    const index = listItems.indexOf(listItem);
    listItems[index].checked = e.target.checked;
    this.setState({ listItems });
    fetch("api/customers/change", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      },
      body: JSON.stringify(listItems[index])
    })
      .then(res => res.json())
      .then(data => console.log(data))
      .catch(err => console.log(err));
    const children =
      e.target.parentElement.parentElement.parentElement.children;
    if (e.target.checked) {
      children[1].style.backgroundColor = "#e9ecef";
      children[1].style.color = "rgba(0, 0, 0, 0.4)";
      children[2].style.backgroundColor = "#e9ecef";
      children[2].style.color = "rgba(0, 0, 0, 0.4)";
    } else {
      children[1].style.backgroundColor = "#fff";
      children[1].style.color = "#495057";
      children[2].style.backgroundColor = "#fff";
      children[2].style.color = "#495057";
    }
  };

  handleAdd = () => {
    const listItems = this.state.listItems;
    listItems.push({
      id:
        "_" +
        Math.random()
          .toString(36)
          .substr(2, 9),
      itemName: "",
      itemQty: "",
      checked: false
    });
    this.setState({ listItems });
    fetch("api/customers", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      },
      body: JSON.stringify(listItems[listItems.length - 1])
    })
      .then(res => res.json())
      .then(data => console.log(data))
      .catch(err => console.log(err));
  };

  handleClearChecked = () => {
    const listItems = this.state.listItems.filter(c => c.checked === false);
    this.setState({ listItems });
  };
  render() {
    return (
      <div className="App">
        <Navbar totalItems={this.state.listItems.length} />
        <List
          listItems={this.state.listItems}
          onDelete={this.handleDelete}
          onNameChange={this.handleNameChange}
          onQtyChange={this.handleQtyChange}
          onChecked={this.handleChecked}
          onAdd={this.handleAdd}
          onClearChecked={this.handleClearChecked}
        />
      </div>
    );
  }
}

export default App;
